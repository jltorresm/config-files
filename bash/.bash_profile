# Change to custom PS1
# export PS1="[\u@\h \W]\\$ " # default one
export PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "

alias ll="ls -alLhH"

alias sublime="/usr/bin/subl"

alias fakeSmtp="java -jar ~/fakeSMTP.jar"

if [ -f /usr/share/bash-completion/completions/git ]; then
	source /usr/share/bash-completion/completions/git
fi

export GOPATH=$(go env GOPATH)
export PATH=$PATH:/home/jose/go/bin

alias perdir="cd ~/Projects/personal"
alias digdir="cd ~/Projects/digintent"
alias kilicodir="cd ~/Projects/kilico"
alias wayvedir="cd ~/Projects/wayve"

# Useful to sign git commits using GPG
export GPG_TTY=$(tty)
export EDITOR=vim

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

# Add flutter to the path
export PATH="$PATH:/home/jose/development/flutter/bin"

# Add ngrok to the path
export PATH="$PATH:/home/jose/development/ngrok"

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

export DOTNET_CLI_TELEMETRY_OPTOUT=1

