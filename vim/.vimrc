" Syntax highlight
syntax on

" Always show cursor position
set ruler

" Show line numbers and highlight them
set number
highlight LineNr ctermfg=black ctermbg=gray

" Show incomplete commands
set showcmd

" In many terminal emulators the mouse works just fine, thus enable it
if has('mouse')
  set mouse=a
endif

" Always set autoindenting on
set autoindent

" Set tab size to 3
set tabstop=3

" Highlight matching brackets
set showmatch

" Set color scheme to monokai pro
set termguicolors
colorscheme monokai_pro

" Set the NERDTree binding
map <C-k><C-b> :NERDTreeToggle<CR>

" Some ctrlp.vim config
let g:ctrlp_custom_ignore = {
    \ 'dir': '\.git\|node_modules$'
\ }

