#Config Files

Repository holding personal configuration files for various apps:

* vim
* sublime
* bash_profile
* apache + hosts
* ssh
* git
