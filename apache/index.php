<?php
	if (isset($_GET['info']))
	{
		phpinfo();
		die;
	}

	echo '<h1>It Totally Works!</h1><br>';

	error_reporting(E_ALL & ~E_STRICT);

	listFolder(__DIR__ . '/personal');

	function listFolder($path)
	{
		$dir = scandir($path);
		$exploded = explode('/', $path);
		$visiblePath = end($exploded);
		echo "<div class='jsTree'><ul><li><a href='/$visiblePath'>$visiblePath</a>";
		printLink($visiblePath, $dir);
		echo "</li></ul></div>";
	}

	function printlink($parent, $scannedDir)
	{
		echo '<ul>';
		foreach ($scannedDir as $key => $value)
		{
			if (strpos($value, '.') === 0 )
			{
				continue;
			}
			echo "<li><a href='$parent/$value'>$value</a></li>";
		}
		echo '</ul>';
	}
?>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.2/themes/default/style.min.css">

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jstree/3.0.2/jstree.min.js"></script>
<script type="text/javascript">
$(function()
{
	$('.jsTree').on('changed.jstree', function (e, data) {
		location.href = data.node.a_attr.href;
	})
	.bind("loaded.jstree", function (e, data) {
		// To open al dirs by deafult uncommment the line below
		// data.instance.open_all();
	})
	.jstree({
		'core' :
		{
			"themes" : { "stripes" : true , 'variant': 'large'},
			"multiple": false,
		}
	});
});
</script>
