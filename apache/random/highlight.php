<!DOCTYPE html>
<html>
<head>
	<title>Highlighter</title>
	<link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/railscasts.css">
	<!-- <link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/kimbie.dark.css"> -->
	<style type="text/css">
		body {
			min-height: 1000px;
		}
		.hljs {
			font-family: Courier New;
			font-size: 12px;
		}
	</style>
</head>
<body>

<pre><code><?= isset($_POST['code']) ? htmlentities($_POST['code']) : '// Here goes the highlighted code' ?></code></pre>

<form method="POST">
	<textarea name="code" placeholder="Put your plain text here" rows=10 cols=100><?= $_POST['code'] ?? '' ?></textarea>
	<br>
	<input type="submit" value="Highlight">
</form>

	<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>
		// hljs.configure({
		// 	tabReplace: '  ', // 2 spaces
		// })
		hljs.initHighlightingOnLoad();
	</script>
</body>
</html>
